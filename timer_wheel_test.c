#include "nano_stopwatch.h"
#include "timer_wheel.h"
#include <stdio.h>
#include <stdlib.h>

#define TIMER_COUNT 10000000ULL
static unsigned long long kMaxDumpCount = 100;
static unsigned long long ticks = 0;

typedef struct
{
    wtimer_t timer;
    int idx;
    int counter;
    int trigger_times;
} test_timer_t;

#define BENCHMARK_PRINT(_nsw, _count, _desc)                             \
    do {                                                                 \
        double ns = nsw_elapsed_ns(&(_nsw));                             \
        printf("%-6s %lld timers take %-10.f ns, avg %-2.f ns\n", _desc, \
            (_count), ns, ns / (_count));                                \
    } while (0)

void debug_dump(int widx, int bidx, wtimer_t* timer) {
    test_timer_t* obj = (test_timer_t*)timer;
    printf(
        "(%d, %d) ticks(%llu) idx(%d) timeout(%llu) repeat(%llu) "
        "counter(%d) trigger_times(%d)\n",
        widx, bidx, ticks, obj->idx, timer->timeout, timer->repeat,
        obj->counter, obj->trigger_times);
}

void on_timer(wtimer_t* timer) {
    static int counter = 0;
    test_timer_t* obj = (test_timer_t*)timer;
    if (obj->counter < 0) {
        obj->counter = counter;
    }
    ++obj->trigger_times;
    ++counter;
    if (TIMER_COUNT < kMaxDumpCount) {
        debug_dump(-1, -1, timer);
    }
}
int main() {
    printf("sizeof(wtimer_mgr_t) = %zu, sizeof(wtimer_t) = %zu\n",
        sizeof(wtimer_mgr_t), sizeof(wtimer_t));
    wtimer_mgr_t timer_mgr;
    timer_mgr_init(&timer_mgr, 0);
    size_t size = sizeof(test_timer_t) * TIMER_COUNT;
    test_timer_t* timers = (test_timer_t*)malloc(size);
    // printf("size = %.2fMb\n", (double)size / (1 << 20));
    memset(timers, 0, size);

    nsw_t nsw = nsw_init();
    for (unsigned long long i = 0; i < TIMER_COUNT; ++i) {
        timers[i].idx = i;
        timers[i].counter = -1;
        timer_start(&timer_mgr, (wtimer_t*)&timers[i], on_timer, i,
            (i % 2) ? 0 : TIMER_COUNT);
    }
    BENCHMARK_PRINT(nsw, TIMER_COUNT, "insert");
    nsw_reset(&nsw);
    int trigger_count = 3;
    unsigned long long tick_count = trigger_count * TIMER_COUNT;
    for (ticks = 0; ticks < tick_count; ++ticks) {
        timer_tick(&timer_mgr, ticks);
    }
    BENCHMARK_PRINT(nsw, tick_count, "tick");
    for (unsigned long long i = 0; i < TIMER_COUNT; ++i) {
        test_timer_t* timer = &timers[i];
        int expected_trigger_count = (timer->idx % 2) ? 1 : trigger_count;
        if (timer->idx != timer->counter ||
            timer->trigger_times != expected_trigger_count) {
            printf(
                "error: timers[%llu].id(%d) != counter(%d) or "
                "trigger_times(%d) "
                "!= expected_trigger_count(%d)\n",
                i, timer->idx, timer->counter, timer->trigger_times,
                expected_trigger_count);
            abort();
        }
    }

    if (TIMER_COUNT < kMaxDumpCount) {
        timer_mgr_debug_dump(&timer_mgr, debug_dump);
    }
    return 0;
}