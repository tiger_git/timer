
#define USE_TIMER_LIST 1
#define USE_TIMER_HEAP 0
#define USE_TIMER_WHEEL 0

#if USE_TIMER_LIST
#include "timer_list.h"
#define timer_t ltimer_t
#define timer_mgr_t ltimer_mgr_t
#elif USE_TIMER_HEAP
#include "timer_heap.h"
#define timer_t htimer_t
#define timer_mgr_t htimer_mgr_t
#elif USE_TIMER_WHEEL
#include "timer_wheel.h"
#define timer_t wtimer_t
#define timer_mgr_t wtimer_mgr_t
#endif

#include <stdio.h>

static int ticks = 0;

void on_timer(timer_t* timer) {
    static int counter = 0;
    printf("ticks(%d) counter(%d)\n", ticks, ++counter);
    if (counter == 10) {
        timer_stop(timer);
    }
}

int main() {
	timer_mgr_t timer_mgr;
    timer_mgr_init(&timer_mgr, 0);
	timer_t timer;
    timer_start(&timer_mgr, &timer, on_timer, 10, 5);
    for (ticks = 0; ticks < 100; ++ticks) {
        timer_tick(&timer_mgr, ticks);
    }
    return 0;
}
