# timer
定时器的几种常见实现方式（双向链表、最小堆、时间轮）

## Usage
```c
// #include "timer_list.h"
// #include "timer_heap.h"
#include "timer_wheel.h"
#include <stdio.h>

static int ticks = 0;

void on_timer(timer_t* timer) {
    static int counter = 0;
    printf("ticks(%d) counter(%d)\n", ticks, ++counter);
    if (counter == 10) {
        timer_stop(timer);
    }
}

int main() {
    timer_mgr_t timer_mgr;
    timer_mgr_init(&timer_mgr, 0);
    // timer_mgr_init(&timer_mgr, timer_heap_less_than, 0); // for timer_heap
    timer_t timer;
    timer_start(&timer_mgr, &timer, on_timer, 10, 5);
    for (ticks = 0; ticks < 100; ++ticks) {
        timer_tick(&timer_mgr, ticks);
    }
    return 0;
}
```

## output:
```
ticks(10) counter(1)
ticks(15) counter(2)
ticks(20) counter(3)
ticks(25) counter(4)
ticks(30) counter(5)
ticks(35) counter(6)
ticks(40) counter(7)
ticks(45) counter(8)
ticks(50) counter(9)
ticks(55) counter(10)
```

## `timer_list.h` 基于双向链表
* 优点：
    * 代码简单易懂
    * 精度高
    * 管理结构简单，节省内存
* 缺点：
    * 执行效率太低
#### 时间复杂度
* timer_start：O(n)
* timer_stop：O(1)
* timer_tick：O(n)

`100000`（十万）timers测试结果：
```
insert 100000 timers take (10265660720)ns, avg (102657)ns
tick 300000 timers take (28241741179)ns, avg (94139)ns
```

## `timer_heap.h` 基于最小堆
* 优点：
    * 精度高
    * 管理结构简单，节省内存
    * 执行效率较高
* 缺点：
    * 无
#### 时间复杂度
* timer_start：O(log(n))
* timer_stop：O(log(n))
* timer_tick：O(log(n))

`10000000`（千万）timers测试结果：
```
insert 10000000 timers take (420046650)ns, avg (42.00)ns
tick 30000000 timers take (8458457291)ns, avg (281.95)ns
```

## `timer_wheel.h` 基于时间轮
* 优点：
    * 执行效率极高
* 缺点：
    * 管理结构略复杂，想提高精度需要牺牲内存
#### 时间复杂度
* timer_start：O(1)
* timer_stop：O(1)
* timer_tick：O(1)

`10000000`（千万）timers测试结果：
```
insert 10000000 timers take (45928439)ns, avg (5)ns
tick 20000000 timers take (409892165)ns, avg (20)ns
```
