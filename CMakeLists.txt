cmake_minimum_required(VERSION 3.14.0)
project(timer)

# compiler setting
message(STATUS "CMAKE_C_COMPILER_ID: ${CMAKE_C_COMPILER_ID}")
if (CMAKE_C_COMPILER_ID MATCHES "MSVC")
    add_compile_options(/WX)
    add_compile_options(/W4)
elseif (CMAKE_C_COMPILER_ID MATCHES "GNU")
    add_compile_options(-Wall -Werror -Wextra )
elseif (CMAKE_C_COMPILER_ID MATCHES ".*Clang")
    add_compile_options(-Wall -Werror -Wextra)
endif ()

set(CMAKE_RUNTIME_OUTPUT_DIRECTORY_DEBUG ${PROJECT_SOURCE_DIR}/bin)
set(CMAKE_RUNTIME_OUTPUT_DIRECTORY_RELEASE ${PROJECT_SOURCE_DIR}/bin)

if (MSVC)
	add_compile_definitions(_CRT_SECURE_NO_WARNINGS)
endif ()

add_executable(timer_list_test nano_stopwatch.h timer_list.h timer_list_test.c)
set_target_properties(timer_list_test PROPERTIES DEBUG_POSTFIX "_d")

add_executable(timer_heap_test nano_stopwatch.h timer_heap.h timer_heap_test.c)
set_target_properties(timer_heap_test PROPERTIES DEBUG_POSTFIX "_d")

add_executable(timer_wheel_test nano_stopwatch.h timer_wheel.h timer_wheel_test.c)
set_target_properties(timer_wheel_test PROPERTIES DEBUG_POSTFIX "_d")

add_executable(timer_usage timer_list.h timer_heap.h timer_wheel.h timer_usage.c)
set_target_properties(timer_usage PROPERTIES DEBUG_POSTFIX "_d")

